<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});



Route::get('/', function()
{
    return View::make('pages.home');
});

Route::get('/login', function () {
    return View::make('pages.login');
});

Route::get('/activityForm', function()
{
    return View::make('pages.activityForm');
});

Route::get('/activityGrid', function()
{
    return View::make('pages.activityGrid');
});

Route::get('/logForm', function()
{
    return View::make('pages.logForm');
});

Route::get('/logGrid', function()
{
    return View::make('pages.logGrid');
});

