@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-10">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <h4 class="mb-0">Logs</h4>
                    </div>
                    <div class="col">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input bg-primary" id="customSwitch1">
                            <label class="custom-control-label" for="customSwitch1">Pending</label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card-body">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Row</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Clark</td>
                            <td>Kent</td>
                            <td>clarkkent@mail.com</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>John</td>
                            <td>Carter</td>
                            <td>johncarter@mail.com</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Peter</td>
                            <td>Parker</td>
                            <td>peterparker@mail.com</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-2">
        <div class="card">
            <div class="card-header">
                <h4 class="mb-0">Search for a log</h4>
            </div>
            <div class="card-body">
                <form>
                    <div class="form-row">
                        <div class="form-group">
                            <label for="inputUserId">UserID</label>
                            <input type="text" class="form-control" id="inputUserId" placeholder="Logger/userID">
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group">
                            <label for="inputLogTitlte">Title</label>
                            <input type="text" class="form-control" id="inputUserName" placeholder="Log Title">
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group">
                            <button type="submit" class="btn bg-secondary">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                Search</button>
                        </div>
                    </div>

            </div>
            </form>
        </div>

    </div>
</div>
@stop
