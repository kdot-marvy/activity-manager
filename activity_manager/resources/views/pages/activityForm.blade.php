@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-10">
        <div class="card">
    <div class="card-header">
        <h4 class="mb-0">Insert Activity</h4>
    </div>
    <div class="card-body">
        <form>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputUserId">Status</label>
                    <select id="inputStatus" class="form-control">
                        <option selected>Pending</option>
                        <option>Done</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="inputUserName">SMS Count</label>
                    <input type="number" class="form-control" id="inputUserName" placeholder="0">
                </div>
            </div>
            <fieldset class="border p-2">
                <legend class="w-auto">User</legend>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputUserId">UserID</label>
                        <input type="text" class="form-control" id="inputUserId" placeholder="Logger/userID">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputUserName">Username</label>
                        <input type="text" class="form-control" id="inputUserName" placeholder="Username">
                    </div>
                </div>
            </fieldset>
            <fieldset class="border p-2">
                <legend class="w-auto">Activity</legend>
                <div class="form-group">
                    <label for="inputActivityTitle">Title</label>
                    <input type="text" class="form-control" id="inputActivityTitle" placeholder="mobile money">
                </div>
                <div class="form-group">
                    <label for="inputActivityRemark">Remark</label>
                    <input type="text" class="form-control" id="inputActivityRemark" placeholder="Remark">
                </div>
            </fieldset>
            <br>
            <button type="submit" class="btn bg-secondary">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>

                Save</button>
        </form>
    </div>
</div>
    </div>
</div>

@stop
